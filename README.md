# baldeweg/site-generator

Writes file content to the filesystem.

## Installation

```shell
composer require baldeweg/site-generator
```

## Usage

```php
use Baldeweg\SiteGenerator\Generator;

$generator = new Generator();
$generator->generate($html, $filename, $format);
```

Security: Please validate all urls before saving a file via this package!

## Dev

- bin/build - Reports and tests
- bin/lint - Checks for code standard violations and fixes them partially
- bin/phpunit - Runs the phpunit tests
- bin/tag VERSION - Sets a git tag
