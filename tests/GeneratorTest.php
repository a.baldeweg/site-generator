<?php

/*
 * This script is part of baldeweg/site-generator
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\SiteGenerator\Tests;

use Baldeweg\SiteGenerator\Generator;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class GeneratorTest extends TestCase
{
    public function testGeneratorWithFileExtension()
    {
        vfsStream::setup('root');

        $generator = new Generator();
        $generator->setTarget(vfsStream::url('root/target'));
        $generator->generate('content', 'slug', 'html');

        $this->assertTrue(
            is_file(
                vfsStream::url('root/target/slug.html')
            )
        );
        $this->assertEquals(
            'content',
            file_get_contents(
                vfsStream::url('root/target/slug.html')
            )
        );
    }

    public function testGeneratorWithoutFileExtension()
    {
        vfsStream::setup('root');

        $generator = new Generator();
        $generator->setTarget(vfsStream::url('root/target'));
        $generator->generate('content', 'slug');

        $this->assertTrue(
            is_file(
                vfsStream::url('root/target/slug')
            )
        );
        $this->assertEquals(
            'content',
            file_get_contents(
                vfsStream::url('root/target/slug')
            )
        );
    }
}
