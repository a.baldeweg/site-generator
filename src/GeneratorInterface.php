<?php

/*
 * This script is part of baldeweg/site-generator
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\SiteGenerator;

interface GeneratorInterface
{
    public function setTarget(string $target): void;

    public function generate(string $html, string $filename, string $format): void;
}
