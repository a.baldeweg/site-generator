<?php

/*
 * This script is part of baldeweg/site-generator
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\SiteGenerator;

use Baldeweg\SiteGenerator\GeneratorInterface;
use Symfony\Component\Filesystem\Filesystem;

class Generator implements GeneratorInterface
{
    private $target = 'build/public';


    public function setTarget(string $target): void
    {
        $this->target = $target;
    }

    public function generate(string $content, string $filename, string $format = null): void
    {
        $fs = new Filesystem();

        if (!$fs->exists($this->target)) {
            $fs->mkdir($this->target);
        }

        file_put_contents(
            $this->target . '/' . $filename . ($format ? '.' . $format : null),
            $content
        );
    }
}
